variables:
  GIT_SUBMODULE_STRATEGY: recursive

stages:
  - test
  - build
  - deploy

test-build:
  stage: test
  image: node:18.12.1
  script:
    - npm install
    - npm run build
  except:
    - staging
    - master

.environment:
  parallel:
    matrix:
      - ENVIRONMENT: ['STG', 'PROD']
  rules:
    - if: $ENVIRONMENT == "PROD" && $CI_COMMIT_BRANCH == 'master'
    - if: $ENVIRONMENT == "STG" && $CI_COMMIT_BRANCH == 'staging'

build:
  stage: build
  image: google/cloud-sdk
  services:
    - docker:dind
  extends:
    - .environment
  script:
    - google_cloud_sdk_login
    - gcloud builds submit . --config=kubernetes/cloudbuild.yaml --substitutions _VERSION=$(echo $CI_COMMIT_SHORT_SHA)

deploy:
  stage: deploy
  image: google/cloud-sdk
  extends:
    - .environment
  script:
    - echo "Deploy to $ENVIRONMENT"
    - google_cloud_sdk_login
    - config_kubernetes
    - create_ssl_secret
    - apply_and_rollout

# Custom Functions -------------------------------------------------------
.custom_functions: &custom_functions |
  export SECRET_NAME=$CI_PROJECT_NAME
  export GCP_PROJECT_ID="${ENVIRONMENT}"_GCP_PROJECT_ID
  export KUBE_NAME="$ENVIRONMENT"_KUBE_NAME
  export GCP_SA_KEY="${ENVIRONMENT}"_GCP_SA_KEY
  export GCP_ZONE="${ENVIRONMENT}"_GCP_ZONE

  function config_kubernetes() {
    gcloud container clusters get-credentials ${!KUBE_NAME} --zone ${!GCP_ZONE} --project ${!GCP_PROJECT_ID}
  }

  function google_cloud_sdk_login() {
    export USE_GKE_GCLOUD_AUTH_PLUGIN=True
    echo ${!GCP_SA_KEY} > gcloud-service-key.json # Google Cloud service accounts
    gcloud auth activate-service-account --key-file gcloud-service-key.json
    gcloud config set project ${!GCP_PROJECT_ID}
  }

  function create_ssl_secret() {
    ## Check if secret name already exists
    if [[ $SECRET_NAME != $(kubectl get secret $SECRET_NAME -o jsonpath="{.metadata.name}") ]]; then 
      kubectl create secret generic $SECRET_NAME
    fi
  }

  function apply_and_rollout() {
    cd "kubernetes/overlays/$ENVIRONMENT"
    curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
    ./kustomize edit set image gcr.io/bharatride/$CI_PROJECT_NAME:latest=gcr.io/${!GCP_PROJECT_ID}/$CI_PROJECT_NAME:$CI_COMMIT_SHORT_SHA
    ./kustomize build . 2>&1 | tee apply.yaml
    kubectl apply -f apply.yaml
    kubectl rollout status deployment.v1.apps/$CI_PROJECT_NAME
    kubectl get all,ing -l app=$CI_PROJECT_NAME
  }

before_script:
  - *custom_functions
