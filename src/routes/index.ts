import { Router } from "express";
import { AddressController } from "../server/controllers/address.controller";

import { appContainer } from "../inversify.config";
import { InterfaceRequestValidationMiddleware } from "../plugins/request-validation.interface";
import { ChildController } from "../server/controllers/child.controller";
import { HealthController } from "../server/controllers/health.controller";
import { SwaggerController } from "../server/controllers/swagger.controller";
import { UserTypeController } from "../server/controllers/user-type.controller";
import { UserController } from "../server/controllers/user.controller";
import { Child } from "../server/models/child";
import { User } from "../server/models/user";
import { Address } from "../server/models/address";

const routes = Router();

const swaggerController = appContainer.get<SwaggerController>(
  nameof<SwaggerController>()
);
const healthController = appContainer.get<HealthController>(
  nameof<HealthController>()
);

const userController = appContainer.get<UserController>(
  nameof<UserController>()
);

const userTypeController = appContainer.get<UserTypeController>(
  nameof<UserTypeController>()
);

const childController = appContainer.get<ChildController>(
  nameof<ChildController>()
);

const addressController = appContainer.get<AddressController>(
  nameof<AddressController>()
);

const requestValidationMiddleware =
  appContainer.get<InterfaceRequestValidationMiddleware>(
    nameof<InterfaceRequestValidationMiddleware>()
  );

const validationMiddleware =
  requestValidationMiddleware.validationMiddleware.bind(
    requestValidationMiddleware
  );

routes.get("/", healthController.getHealth.bind(healthController));

routes.get("/health", healthController.getHealth.bind(healthController));

routes.get("/api", swaggerController.getDocs.bind(swaggerController));

/**
 * @swagger
 * /child-drivers/:
 *  get:
 *   description: Get the child drivers by phone number
 *   summary: Returns child drivers object by phone number
 *   tags: [User]
 *   responses:
 *     200:
 *       description: Returns child drivers object by phone number
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *              $ref: '#/components/schemas/UserResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  "/child-drivers/",
  userController.getChildDrivers.bind(userController)
);

/**
 * @swagger
 * /user/:
 *  get:
 *   description: Get the user by phone number
 *   summary: Returns user object by phone number
 *   tags: [User]
 *   responses:
 *     200:
 *       description: Returns user object by phone number
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/UserWithAddressResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get("/user/", userController.getByPhoneNumber.bind(userController));

/**
 * @swagger
 * /users/:
 *  post:
 *   description: Get the user by ids
 *   summary: Returns user object by ids
 *   tags: [User]
 *   requestBody:
 *     description: User ids
 *     required: true
 *     content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/GetUserByIds'
 *   responses:
 *     200:
 *       description: Returns user object by ids
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *                $ref: '#/components/schemas/UserResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  "/users/",
  [
    validationMiddleware("body", User, {
      validator: { groups: ["getByUserIds"] },
    }),
  ],
  userController.getByUserIds.bind(userController)
);

/**
 * @swagger
 * /user/:
 *  post:
 *   description: Create a user as driver and/or parent
 *   summary: Returns user object
 *   tags: [User]
 *   requestBody:
 *     description: User body with address
 *     required: true
 *     content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreateUserWithAddressBody'
 *   responses:
 *     200:
 *       description: Returns user object
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UserResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  "/user/",
  [
    validationMiddleware("body", User, {
      validator: { groups: ["create"] },
    }),
  ],
  userController.create.bind(userController)
);

/**
 * @swagger
 * /user/:
 *  put:
 *   description: Update a user as driver and/or parent
 *   summary: Returns number of user updated
 *   tags: [User]
 *   requestBody:
 *     description: User body with address
 *     required: true
 *     content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/UpdateUserWithAddressBody'
 *   responses:
 *     200:
 *       description: Returns number of user object updated
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UpdateRecordsResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.patch(
  "/user/",
  [
    validationMiddleware("body", User, {
      validator: { groups: ["update"] },
    }),
  ],
  userController.update.bind(userController)
);

/**
 * @swagger
 * /user/:
 *  delete:
 *   description: Delete the user by phone number
 *   summary: Returns number of user deleted by phone number
 *   tags: [User]
 *   responses:
 *     200:
 *       description: Returns number of user object deleted by phone number
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/DeleteRecordsResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.delete(
  "/user/",
  userController.deleteByPhoneNumber.bind(userController)
);

/**
 * @swagger
 * /user-type/:
 *  get:
 *   description: Get the user-type by phone number
 *   summary: Returns user-type object by phone number
 *   tags: [User Type]
 *   responses:
 *     200:
 *       description: Returns user-type object by phone number
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UserTypeWithUserWithAddressResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  "/user-type/",
  userTypeController.getByPhoneNumber.bind(userTypeController)
);

/**
 * @swagger
 * /user-type/add-role/:
 *  patch:
 *   description: Add a new role to the user
 *   summary: Returns true indicating the role has been added to the user
 *   tags: [User Type]
 *   requestBody:
 *     description: User with role
 *     required: true
 *     content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/UserAddRole'
 *   responses:
 *     200:
 *       description: Returns true indicating the role has been added to the user
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UpdateRecordsResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.patch(
  "/user-type/add-role/",
  [
    validationMiddleware("body", User, {
      validator: { groups: ["add-role"] },
    }),
  ],
  userTypeController.addRole.bind(userTypeController)
);

/**
 * @swagger
 * /childs/:
 *  get:
 *   description: Get all the childs by phone number
 *   summary: Returns an array of child object by phone number
 *   tags: [Child]
 *   responses:
 *     200:
 *       description: Returns an array of child object by phone number
 *       content:
 *         application/json:
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/components/schemas/ChildResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get("/childs/", childController.getByPhoneNumber.bind(childController));

/**
 * @swagger
 * /child/:
 *  post:
 *   description: Create a child for a parent
 *   summary: Returns newly created child
 *   tags: [Child]
 *   requestBody:
 *     description: Child body
 *     required: true
 *     content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreateChildBody'
 *   responses:
 *     200:
 *       description: Returns newly created child
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ChildResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  "/child/",
  [
    validationMiddleware("body", Child, {
      validator: { groups: ["createChild"] },
    }),
  ],
  childController.create.bind(childController)
);

/**
 * @swagger
 * /child/:
 *  put:
 *   description: Update a child for a parent
 *   summary: Returns number of child updated
 *   tags: [Child]
 *   requestBody:
 *     description: Child body
 *     required: true
 *     content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/UpdateChildBody'
 *   responses:
 *     200:
 *       description: Returns number of child udpated
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UpdateRecordsResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.put(
  "/child/",
  [
    validationMiddleware("body", Child, {
      validator: { groups: ["updateChild"] },
    }),
  ],
  childController.update.bind(childController)
);

/**
 * @swagger
 * /child/{id}:
 *  delete:
 *   description: Delete a child by id
 *   summary: Returns number of child deleted
 *   tags: [Child]
 *   parameters:
 *     - name: id
 *       description: Child id
 *       in: path
 *       required: true
 *       schema:
 *        type: number
 *   responses:
 *     200:
 *       description: Returns number of child deleted
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/DeleteRecordsResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.delete(
  "/child/:id",
  [
    validationMiddleware("params", Child, {
      validator: { groups: ["deleteChildById"] },
    }),
  ],
  childController.deleteById.bind(childController)
);

/**
 * @swagger
 * /address/{id}/:
 *  get:
 *   description: Get the address by id
 *   summary: Returns address object by id
 *   tags: [Address]
 *   parameters:
 *     - name: id
 *       description: Address id
 *       in: path
 *       required: true
 *       schema:
 *        type: number
 *   responses:
 *     200:
 *       description: Returns address object by id
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/AddressResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  "/address/:id/",
  [
    validationMiddleware("params", Address, {
      validator: { groups: ["getById"] },
    }),
  ],
  addressController.getById.bind(addressController)
);

/**
 * @swagger
 * /address/:
 *  post:
 *   description: Create an address
 *   summary: Returns address object
 *   tags: [Address]
 *   requestBody:
 *     description: Address body
 *     required: true
 *     content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreateAddressBody'
 *   responses:
 *     200:
 *       description: Returns address object
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/AddressResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  "/address/",
  [
    validationMiddleware("body", Address, {
      validator: { groups: ["createAddress"] },
    }),
  ],
  addressController.create.bind(addressController)
);

/**
 * @swagger
 * /address/:
 *  put:
 *   description: Update an address
 *   summary: Returns number of address updated
 *   tags: [Address]
 *   requestBody:
 *     description: Address body
 *     required: true
 *     content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/UpdateAddressBody'
 *   responses:
 *     200:
 *       description: Returns number of address object updated
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UpdateRecordsResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.put(
  "/address/",
  [
    validationMiddleware("body", Address, {
      validator: { groups: ["updateAddress"] },
    }),
  ],
  addressController.update.bind(addressController)
);

/**
 * @swagger
 * /address/{id}/:
 *  delete:
 *   description: Delete the address by id
 *   summary: Returns number of address deleted by id
 *   tags: [Address]
 *   parameters:
 *     - name: id
 *       description: Address id
 *       in: path
 *       required: true
 *       schema:
 *        type: number
 *   responses:
 *     200:
 *       description: Returns number of Address object deleted by id
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/DeleteRecordsResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.delete(
  "/address/:id/",
  [
    validationMiddleware("params", Address, {
      validator: { groups: ["deleteById"] },
    }),
  ],
  addressController.deleteById.bind(addressController)
);
export { routes };
