import { Config } from '../server/models/config';

export const localConfig: Config = {
  serviceConfig: {
    name: 'User API Microservice',
    environment: 'local',
    namespace: 'user-api',
    host: 'localhost',
    description: 'Presentation api to call internal microservices',
    port: 5555,
  },
  googleCloudConfig: {
    projectId: 'rock-verbena-322706',
    secrets: {
      database: 'local-db',
      firebase: 'firebase-service-account',
    },
    firestoreCollection: 'stagingTimestamp',
  },
  endpoints: {
    userApi: 'https://group-api.stg.bharatride.in',
    deviceApi: 'https://device-api.stg.bharatride.in',
  },
};
