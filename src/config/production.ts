import { Config } from '../server/models/config';

export const productionConfig: Config = {
  serviceConfig: {
    name: 'User API Microservice',
    environment: 'production',
    namespace: 'user-api',
    host: 'production.user-api.api.bharatride.in',
    description: 'Presentation api to call internal microservices',
    port: 5555,
  },
  googleCloudConfig: {
    projectId: 'ride-api-prod',
    secrets: {
      database: 'database',
      firebase: 'firebase-service-account',
    },
    firestoreCollection: 'productionTimestamp',
  },
  endpoints: {
    userApi: 'https://group-api.prod.bharatride.in',
    deviceApi: 'https://device-api.prod.bharatride.in',
  },
};
