import { Address } from '../models/address';

export interface AddressServiceInterface {
  getById(id: number): Promise<Address | null>;
  create(address: Address): Promise<Address>;
  update(address: Address): Promise<[number]>;
  deleteById(id: number): Promise<number>;
}
