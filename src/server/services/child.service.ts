import { inject, injectable } from 'inversify';

import { Child } from '../models/child';
import { AddressDataAccessInterface } from '../repository/address.interface';
import { ChildDataAccessInterface } from '../repository/child.interface';
import { logger } from '../utils/logger';
import { ChildServiceInterface } from './child.interface';

@injectable()
export class ChildService implements ChildServiceInterface {
  constructor(
    @inject(nameof<ChildDataAccessInterface>())
    private _childDataAccess: ChildDataAccessInterface,
    @inject(nameof<AddressDataAccessInterface>())
    private _addressDataAccess: AddressDataAccessInterface
  ) {}

  async getByPhoneNumber(phoneNumber: string): Promise<Child[] | null> {
    return this._childDataAccess.getByPhoneNumber(phoneNumber);
  }

  async create(child: Child): Promise<Child> {
    await this.checkForUserAddress(child);

    return this._childDataAccess.create(child);
  }

  private async checkForUserAddress(child: Child): Promise<void> {
    if (!child.addressId) {
      if (!child.address) {
        logger.info('No address provided for the child', { child });
      }
      if (child.address) {
        // Create address and add the id of the address to the user
        child.addressId = (
          await this._addressDataAccess.create(child.address)
        ).id;
      }
    }
  }

  async update(child: Child): Promise<[number]> {
    await this.checkForUserAddress(child);

    return this._childDataAccess.partialUpdate(child);
  }

  async deleteById(id: number): Promise<number> {
    return this._childDataAccess.deleteById(id);
  }
}
