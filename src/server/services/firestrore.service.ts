import { injectable } from "inversify";
import { FirestoreServiceInterface } from "./firestore.interface";
import * as admin from "firebase-admin";
import moment from "moment";
import { config } from "../../config";

@injectable()
export class FirestoreService implements FirestoreServiceInterface {
  async updateUserTimestampByUserIds(userIds: number[]): Promise<void> {
    if (userIds && userIds.length) {
      const promises: Promise<FirebaseFirestore.WriteResult>[] = [];
      userIds.forEach((userId) => {
        promises.push(
          admin
            .firestore()
            .collection(config.googleCloudConfig.firestoreCollection)
            .doc(`userId_${userId}`)
            .set({ user: moment.utc().valueOf() }, { merge: true })
        );
      });
      await Promise.all(promises);
    }
  }

  async updateChildTimestampByUserIds(userIds: number[]): Promise<void> {
    if (userIds && userIds.length) {
      const promises: Promise<FirebaseFirestore.WriteResult>[] = [];
      userIds.forEach((userId) => {
        promises.push(
          admin
            .firestore()
            .collection(config.googleCloudConfig.firestoreCollection)
            .doc(`userId_${userId}`)
            .set({ child: moment.utc().valueOf() }, { merge: true })
        );
      });
      await Promise.all(promises);
    }
  }
}
