import { inject, injectable } from "inversify";

import { User } from "../models/user";
import { UserType } from "../models/user-type";
import { UserTypeEnum } from "../models/user-type.enum";
import { AddressDataAccessInterface } from "../repository/address.interface";
import { UserTypeDataAccessInterface } from "../repository/user-type.interface";
import { UserDataAccessInterface } from "../repository/user.interface";
import { logger } from "../utils/logger";
import { UserServiceInterface } from "./user.interface";

@injectable()
export class UserService implements UserServiceInterface {
  constructor(
    @inject(nameof<UserDataAccessInterface>())
    private _userDataAccess: UserDataAccessInterface,
    @inject(nameof<AddressDataAccessInterface>())
    private _addressDataAccess: AddressDataAccessInterface,
    @inject(nameof<UserTypeDataAccessInterface>())
    private _userTypeDataAccess: UserTypeDataAccessInterface
  ) {}

  async getById(id: number): Promise<User | null> {
    return this._userDataAccess.getById(id);
  }

  async getByUserIds(ids: number[]): Promise<User[]> {
    return this._userDataAccess.getByUserIds(ids);
  }

  async getByPhoneNumber(phoneNumber: string): Promise<User | null> {
    return this._userDataAccess.getByPhoneNumber(phoneNumber);
  }

  async create(user: User): Promise<User> {
    await this.checkForUserAddress(user);

    const createdUser = await this._userDataAccess.create(user);

    // Create userType and set the user to driver
    const userType = new UserType();
    userType.isDriver =
      user.role === UserTypeEnum.DRIVER || user.role === UserTypeEnum.BOTH;
    userType.isParent =
      user.role === UserTypeEnum.PARENT || user.role === UserTypeEnum.BOTH;
    userType.userId = createdUser.id;
    await this._userTypeDataAccess.create(userType);

    // Return the created user
    return createdUser;
  }

  async update(user: User): Promise<[number]> {
    this.checkForUserAddress(user);
    return this._userDataAccess.partialUpdate(user);
  }

  async deleteById(id: number): Promise<number> {
    return this._userDataAccess.deleteById(id);
  }

  async deleteByPhoneNumber(phoneNumber: string): Promise<number> {
    const userType = await this._userTypeDataAccess.getByPhoneNumber(
      phoneNumber
    );
    if (userType) {
      await this._userTypeDataAccess.deleteById(userType.id);
      await this._userDataAccess.deleteById(userType.userId);
      return 1;
    }
    return 0;
  }

  async getChildDriversByParentId(parentId: number): Promise<User[]> {
    return this._userDataAccess.getChildDriversByParentId(parentId);
  }

  private async checkForUserAddress(user: User): Promise<void> {
    if (!user.addressId) {
      if (!user.address) {
        logger.info("No address provided for the user", { user });
      }
      if (user.address) {
        // Create address and add the id of the address to the user
        user.addressId = (
          await this._addressDataAccess.create(user.address)
        ).id;
      }
    }
  }
}
