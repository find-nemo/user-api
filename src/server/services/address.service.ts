import { inject, injectable } from 'inversify';

import { Address } from '../models/address';
import { AddressDataAccessInterface } from '../repository/address.interface';
import { AddressServiceInterface } from './address.interface';

@injectable()
export class AddressService implements AddressServiceInterface {
  constructor(
    @inject(nameof<AddressDataAccessInterface>())
    private _addressDataAccess: AddressDataAccessInterface
  ) {}

  async getById(id: number): Promise<Address | null> {
    return this._addressDataAccess.getById(id);
  }

  async create(address: Address): Promise<Address> {
    return this._addressDataAccess.create(address);
  }

  async update(address: Address): Promise<[number]> {
    return this._addressDataAccess.update(address);
  }

  async deleteById(id: number): Promise<number> {
    return this._addressDataAccess.deleteById(id);
  }
}
