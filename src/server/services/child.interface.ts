import { Child } from '../models/child';

export interface ChildServiceInterface {
  getByPhoneNumber(phoneNumber: string): Promise<Child[] | null>;
  create(child: Child): Promise<Child>;
  update(child: Child): Promise<[number]>;
  deleteById(childId: number): Promise<number>;
}
