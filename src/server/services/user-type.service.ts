import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { UserType } from '../models/user-type';
import { UserTypeEnum } from '../models/user-type.enum';
import { UserTypeDataAccessInterface } from '../repository/user-type.interface';
import { UserDataAccessInterface } from '../repository/user.interface';
import { UserTypeServiceInterface } from './user-type.interface';

@injectable()
export class UserTypeService implements UserTypeServiceInterface {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<UserTypeDataAccessInterface>())
    private _userTypeDataAccess: UserTypeDataAccessInterface,
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin,
    @inject(nameof<UserDataAccessInterface>())
    private _userDataAccess: UserDataAccessInterface
  ) {
    this._error = errorFactory(nameof<UserTypeService>());
  }

  async getByPhoneNumber(phoneNumber: string): Promise<UserType | null> {
    return this._userTypeDataAccess.getByPhoneNumber(phoneNumber);
  }

  async addRole(
    role: UserTypeEnum,
    phoneNumber: string
  ): Promise<[number]> {
    const user = await this._userDataAccess.getByPhoneNumber(phoneNumber);
    if (user) {
      return await this._userTypeDataAccess.addRole(role, user?.id);
    }
    throw this._error.getFormattedError({
      source: 'int',
      status: 400,
      message: 'User does not exists by phoneNumber',
      errorData: {
        role,
        phoneNumber,
      },
    });
  }
}
