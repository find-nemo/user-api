export interface FirestoreServiceInterface {
  updateUserTimestampByUserIds(userIds: number[]): Promise<void>;
  updateChildTimestampByUserIds(userIds: number[]): Promise<void>;
}
