import { UserType } from '../models/user-type';
import { UserTypeEnum } from '../models/user-type.enum';

export interface UserTypeServiceInterface {
  getByPhoneNumber(phoneNumber: string): Promise<UserType | null>;
  addRole(
    role: UserTypeEnum,
    phoneNumber: string
  ): Promise<[number]>;
}
