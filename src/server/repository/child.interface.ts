import { Child } from '../models/child';

export interface ChildDataAccessInterface {
  getByPhoneNumber(phoneNumber: string): Promise<Child[] | null>;
  create(child: Child): Promise<Child>;
  partialUpdate(child: Child): Promise<[number]>;
  deleteById(childId: number): Promise<number>;
}
