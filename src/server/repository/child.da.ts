import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Address } from '../models/address';
import { Child } from '../models/child';
import { User } from '../models/user';
import { ChildDataAccessInterface } from './child.interface';

@injectable()
export class ChildDataAccess implements ChildDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<ChildDataAccess>());
  }

  async getByPhoneNumber(phoneNumber: string): Promise<Child[] | null> {
    try {
      return await Child.findAll({
        include: [
          {
            model: User,
            where: {
              phoneNumber,
            },
          },
          {
            model: Address,
          },
        ],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting childs by phoneNumber',
        source: 'intdb',
        errorData: {
          error,
          phoneNumber,
        },
      });
    }
  }

  async create(child: Child): Promise<Child> {
    try {
      if (child.save) {
        return await child.save();
      }
      return await Child.create(child);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while creating child',
        source: 'intdb',
        errorData: {
          error,
          child,
        },
      });
    }
  }

  async partialUpdate(child: Child): Promise<[number]> {
    try {
      const updateValues: Partial<Child> = {};
      if (child.fullName) {
        updateValues.fullName = child.fullName;
      }
      if (child.addressId) {
        updateValues.addressId = child.addressId;
      }
      if (child.profileImageUrl) {
        updateValues.profileImageUrl = child.profileImageUrl;
      }
      if (child.parentId) {
        updateValues.parentId = child.parentId;
      }
      return await Child.update(updateValues, { where: { id: child.id } });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while updating child',
        source: 'intdb',
        errorData: {
          error,
          child,
        },
      });
    }
  }

  async deleteById(childId: number): Promise<number> {
    try {
      return await Child.destroy({ where: { id: childId } });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while deleting child by id',
        source: 'intdb',
        errorData: {
          error,
          childId,
        },
      });
    }
  }
}
