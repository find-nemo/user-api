import { id, inject, injectable } from "inversify";
import { Op } from "sequelize";

import { InterfaceErrorHandlerPlugin } from "../../plugins/error-handler.interface";
import { Address } from "../models/address";
import { Child } from "../models/child";
import { ChildGroup } from "../models/child-group";
import { DriverGroup } from "../models/driver-group";
import { User } from "../models/user";
import { AddressDataAccess } from "./address.da";
import { UserDataAccessInterface } from "./user.interface";

@injectable()
export class UserDataAccess implements UserDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<AddressDataAccess>());
  }

  async getById(id: number): Promise<User | null> {
    try {
      return await User.findByPk(1);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting user by id",
        source: "intdb",
        errorData: {
          error,
          userId: id,
        },
      });
    }
  }

  async getByUserIds(ids: number[]): Promise<User[]> {
    try {
      return await User.findAll({ where: { id: { [Op.in]: ids } } });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting user by id",
        source: "intdb",
        errorData: {
          error,
          userId: id,
        },
      });
    }
  }

  async getByPhoneNumber(phoneNumber: string): Promise<User | null> {
    try {
      return await User.findOne({
        where: { phoneNumber },
        include: [{ model: Address }],
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while getting user by phoneNumber",
        source: "intdb",
        errorData: {
          error,
          phoneNumber,
        },
      });
    }
  }

  async create(user: User): Promise<User> {
    try {
      if (user.save) {
        return await user.save();
      }
      return await User.create(user);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while creating user",
        source: "intdb",
        errorData: {
          error,
          user,
        },
      });
    }
  }

  async partialUpdate(user: User): Promise<[number]> {
    try {
      const updateValues: Partial<User> = {};
      if (user.fullName) {
        updateValues.fullName = user.fullName;
      }
      if (user.addressId) {
        updateValues.addressId = user.addressId;
      }
      if (user.profileImageUrl) {
        updateValues.profileImageUrl = user.profileImageUrl;
      }
      if (user.photoIdUrl) {
        updateValues.photoIdUrl = user.photoIdUrl;
      }
      if (user.email) {
        updateValues.email = user.email;
      }
      return await User.update(updateValues, {
        where: { phoneNumber: user.phoneNumber },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while updating user",
        source: "intdb",
        errorData: {
          error,
          user,
        },
      });
    }
  }

  async deleteById(id: number): Promise<number> {
    try {
      return await User.destroy({
        where: {
          id,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while deleting user by id",
        source: "intdb",
        errorData: {
          error,
          userId: id,
        },
      });
    }
  }

  async deleteByPhoneNumber(phoneNumber: string): Promise<number> {
    try {
      return await User.destroy({
        where: {
          phoneNumber,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: "Error while deleting user by phoneNumber",
        source: "intdb",
        errorData: {
          error,
          phoneNumber,
        },
      });
    }
  }

  async getChildDriversByParentId(parentId: number): Promise<User[]> {
    const childs: Child[] = await Child.findAll({ where: { parentId } });
    if (!childs || !childs.length) {
      return [];
    }
    const childGroups: ChildGroup[] = await ChildGroup.findAll({
      where: { childId: { [Op.in]: childs.map((c) => c.id) } },
    });
    if (!childGroups || !childGroups.length) {
      return [];
    }
    const driverGroups: DriverGroup[] = await DriverGroup.findAll({
      where: { groupId: { [Op.in]: childGroups.map((c) => c.groupId) } },
      include: [{ model: User }],
    });
    if (!driverGroups || !driverGroups.length) {
      return [];
    }
    return driverGroups.map((d) => d.driver);
  }
}
