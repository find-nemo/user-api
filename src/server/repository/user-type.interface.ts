import { UserType } from '../models/user-type';
import { UserTypeEnum } from '../models/user-type.enum';

export interface UserTypeDataAccessInterface {
  getByPhoneNumber(phoneNumber: string): Promise<UserType | null>;
  create(userType: UserType): Promise<UserType | null>;
  deleteById(id: number): Promise<number>;
  addRole(role: UserTypeEnum, userId: number): Promise<[number]>;
}
