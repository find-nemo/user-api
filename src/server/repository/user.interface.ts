import { User } from "../models/user";

export interface UserDataAccessInterface {
  getById(id: number): Promise<User | null>;
  getByUserIds(ids: number[]): Promise<User[]>;
  getByPhoneNumber(phoneNumber: string): Promise<User | null>;
  create(user: User): Promise<User>;
  partialUpdate(user: User): Promise<[number]>;
  deleteById(id: number): Promise<number>;
  deleteByPhoneNumber(phoneNumber: string): Promise<number>;
  getChildDriversByParentId(parentId: number): Promise<User[]>;
}
