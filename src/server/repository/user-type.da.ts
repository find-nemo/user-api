import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Address } from '../models/address';
import { User } from '../models/user';
import { UserType } from '../models/user-type';
import { UserTypeEnum } from '../models/user-type.enum';
import { AddressDataAccess } from './address.da';
import { UserTypeDataAccessInterface } from './user-type.interface';

@injectable()
export class UserTypeDataAccess implements UserTypeDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<AddressDataAccess>());
  }

  async getByPhoneNumber(phoneNumber: string): Promise<UserType | null> {
    try {
      return await UserType.findOne({
        include: [
          {
            model: User,
            where: {
              phoneNumber
            },
            include: [
              {
                model: Address
              }
            ]
          }
        ]
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting userType by phoneNumber',
        source: 'intdb',
        errorData: {
          error,
          phoneNumber
        }
      });
    }
  }

  async create(userType: UserType): Promise<UserType | null> {
    try {
      if (userType.save) {
        return await userType.save();
      }
      return await UserType.create(userType);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while creating userType',
        source: 'intdb',
        errorData: {
          error,
          userType
        }
      });
    }
  }

  async deleteById(id: number): Promise<number> {
    try {
      return await UserType.destroy({
        where: {
          id
        }
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while deleting userType by id',
        source: 'intdb',
        errorData: {
          error,
          id
        }
      });
    }
  }

  async addRole(
    role: UserTypeEnum,
    userId: number
  ): Promise<[number]> {
    try {
      const updateOptions = {} as UserType;
      if (role === UserTypeEnum.BOTH) {
        updateOptions.isDriver = true;
        updateOptions.isParent = true;
      } else if (role === UserTypeEnum.DRIVER) {
        updateOptions.isDriver = true;
      } else if (role === UserTypeEnum.PARENT) {
        updateOptions.isParent = true;
      }
      return await UserType.update(updateOptions, { where: { userId } });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while updating userType by phoneNumber',
        source: 'intdb',
        errorData: {
          error,
          role,
          userId
        }
      });
    }
  }
}
