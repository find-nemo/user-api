import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Address } from '../models/address';
import { AddressDataAccessInterface } from './address.interface';

@injectable()
export class AddressDataAccess implements AddressDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<AddressDataAccess>());
  }

  async getById(id: number): Promise<Address | null> {
    try {
      return await Address.findByPk(id);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting address by id',
        source: 'intdb',
        errorData: {
          error,
          addressId: id,
        },
      });
    }
  }

  async create(address: Address): Promise<Address> {
    try {
      if (address.save) {
        return await address.save();
      }
      return await Address.create(address);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while creating address',
        source: 'intdb',
        errorData: {
          error,
          address,
        },
      });
    }
  }

  async update(address: Address): Promise<[number]> {
    try {
      return await Address.update(
        {
          address1: address.address1,
          address2: address.address2,
          address3: address.address3,
          city: address.city,
          country: address.country,
          state: address.state,
          postalCode: address.postalCode,
          latitude: address.latitude,
          longitude: address.longitude,
        },
        { where: { id: address.id } }
      );
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while updating address',
        source: 'intdb',
        errorData: {
          error,
          address,
        },
      });
    }
  }

  async deleteById(id: number): Promise<number> {
    try {
      return await Address.destroy({
        where: {
          id,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while deleting address',
        source: 'intdb',
        errorData: {
          error,
          addressId: id,
        },
      });
    }
  }
}
