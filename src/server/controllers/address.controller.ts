import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import { Address } from '../models/address';

import {
  RequestWithBody,
  RequestWithParams,
} from '../models/request-validation';
import { AddressServiceInterface } from '../services/address.interface';

@injectable()
export class AddressController {
  constructor(
    @inject(nameof<AddressServiceInterface>())
    private _addressService: AddressServiceInterface
  ) {}

  async getById(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<Address>;
    res.json(await this._addressService.getById(request.validatedParams.id));
    next();
  }

  async create(req: Request, res: Response, next: NextFunction): Promise<void> {
    const request = req as RequestWithBody<Address>;
    res.json(await this._addressService.create(request.body));
    next();
  }

  async update(req: Request, res: Response, next: NextFunction): Promise<void> {
    const request = req as RequestWithBody<Address>;
    res.json({
      updated: (await this._addressService.update(request.validatedBody))[0],
    });

    next();
  }

  async deleteById(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<Address>;
    res.json({
      deleted: await this._addressService.deleteById(
        request.validatedParams.id
      ),
    });

    next();
  }
}
