import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Child } from '../models/child';
import {
  RequestWithBody,
  RequestWithParams,
  RequestWithPhoneNumber,
} from '../models/request-validation';
import { ChildServiceInterface } from '../services/child.interface';
import { UserServiceInterface } from '../services/user.interface';
import { FirestoreServiceInterface } from '../services/firestore.interface';
import { User } from '../models/user';

@injectable()
export class ChildController {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<ChildServiceInterface>())
    private _childService: ChildServiceInterface,
    @inject(nameof<UserServiceInterface>())
    private _userService: UserServiceInterface,
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin,
    @inject(nameof<FirestoreServiceInterface>())
    private _firestoreService: FirestoreServiceInterface
  ) {
    this._error = errorFactory(nameof<ChildController>());
  }

  async getByPhoneNumber(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber;
    res.json(
      await this._childService.getByPhoneNumber(request.validatedPhoneNumber)
    );
    next();
  }

  async create(req: Request, res: Response, next: NextFunction): Promise<void> {
    const request = req as RequestWithPhoneNumber & RequestWithBody<Child>;
    const phoneNumber =
      request.validatedBody.parentPhoneNumber || request.validatedPhoneNumber;
    let user = await this._userService.getByPhoneNumber(phoneNumber);
    const child = request.validatedBody;

    if (!user) {
      if (phoneNumber) {
        const newUser = new User();
        newUser.fullName = '';
        newUser.phoneNumber = phoneNumber;
        user = await this._userService.create(newUser);
      } else {
        throw this._error.getFormattedError({
          source: 'int',
          status: 400,
          message: 'Cannot find user by phone number',
          errorData: {
            phoneNumber: request.validatedPhoneNumber,
            body: request.validatedBody,
          },
        });
      }
    }

    if (user) {
      child.parentId = user.id;
    }

    res.json(await this._childService.create(child));
    if (user) {
      await this._firestoreService.updateChildTimestampByUserIds([user.id]);
    }
    next();
  }

  async update(req: Request, res: Response, next: NextFunction): Promise<void> {
    const request = req as RequestWithPhoneNumber & RequestWithBody<Child>;
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );
    const child = request.validatedBody;

    if (user) {
      child.parentId = user.id;
    } else {
      throw this._error.getFormattedError({
        source: 'int',
        status: 400,
        message: 'Cannot find user by phone number',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
          body: request.validatedBody,
        },
      });
    }

    res.json({ updated: (await this._childService.update(child))[0] });
    if (user) {
      await this._firestoreService.updateChildTimestampByUserIds([user.id]);
    }
    next();
  }

  async deleteById(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithParams<Child>;
    res.json({
      deleted: await this._childService.deleteById(request.validatedParams.id),
    });
    next();
  }
}
