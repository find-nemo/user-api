import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';

import {
  RequestWithBody,
  RequestWithPhoneNumber,
} from '../models/request-validation';
import { User } from '../models/user';
import { UserTypeServiceInterface } from '../services/user-type.interface';
import { FirestoreServiceInterface } from '../services/firestore.interface';

@injectable()
export class UserTypeController {
  constructor(
    @inject(nameof<UserTypeServiceInterface>())
    private _userTypeService: UserTypeServiceInterface,
    @inject(nameof<FirestoreServiceInterface>())
    private _firestoreService: FirestoreServiceInterface
  ) {}

  async getByPhoneNumber(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber;
    res.json(
      await this._userTypeService.getByPhoneNumber(request.validatedPhoneNumber)
    );
    next();
  }

  async addRole(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithBody<User> & RequestWithPhoneNumber;
    if (request.validatedBody.role) {
      res.json({
        updated: (
          await this._userTypeService.addRole(
            request.validatedBody.role,
            request.validatedPhoneNumber
          )
        )[0],
      });
      const data = await this._userTypeService.getByPhoneNumber(
        request.validatedPhoneNumber
      );
      if (data) {
        await this._firestoreService.updateUserTimestampByUserIds([
          data.userId,
        ]);
      }
    } else {
      res.json({ updated: 0 });
    }
    next();
  }
}
