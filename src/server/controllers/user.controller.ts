import { NextFunction, Request, Response } from "express";
import { inject, injectable } from "inversify";

import {
  RequestWithBody,
  RequestWithPhoneNumber,
} from "../models/request-validation";
import { User } from "../models/user";
import { UserServiceInterface } from "../services/user.interface";
import { FirestoreServiceInterface } from "../services/firestore.interface";
import { InterfaceErrorHandlerPlugin } from "../../plugins/error-handler.interface";

@injectable()
export class UserController {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<UserServiceInterface>())
    private _userService: UserServiceInterface,
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin,
    @inject(nameof<FirestoreServiceInterface>())
    private _firestoreService: FirestoreServiceInterface
  ) {
    this._error = errorFactory(nameof<UserController>());
  }

  async getByPhoneNumber(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber;
    res.json(
      await this._userService.getByPhoneNumber(request.validatedPhoneNumber)
    );
    next();
  }

  async getByUserIds(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithBody<User>;
    res.json(
      await this._userService.getByUserIds(request.validatedBody.userIds)
    );
    next();
  }

  async create(req: Request, res: Response, next: NextFunction): Promise<void> {
    const request = req as RequestWithBody<User> & RequestWithPhoneNumber;
    request.validatedBody.phoneNumber = request.validatedPhoneNumber;
    const data = await this._userService.create(request.validatedBody);
    res.json(data);
    await this._firestoreService.updateUserTimestampByUserIds([data.id]);
    next();
  }

  async update(req: Request, res: Response, next: NextFunction): Promise<void> {
    const request = req as RequestWithBody<User> & RequestWithPhoneNumber;
    request.validatedBody.phoneNumber = request.validatedPhoneNumber;
    res.json({
      updated: (await this._userService.update(request.validatedBody))[0],
    });
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );
    if (user) {
      await this._firestoreService.updateUserTimestampByUserIds([user.id]);
    }
    next();
  }

  async deleteByPhoneNumber(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber;
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );
    res.json({
      deleted: await this._userService.deleteByPhoneNumber(
        request.validatedPhoneNumber
      ),
    });
    if (user) {
      await this._firestoreService.updateUserTimestampByUserIds([user.id]);
    }
    next();
  }

  async getChildDrivers(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber;
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );
    if (!user) {
      throw this._error.getFormattedError({
        source: "extapi",
        status: 400,
        message: "Cannot find user by phoneNumber",
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
        },
      });
    }
    res.json(await this._userService.getChildDriversByParentId(user?.id));

    next();
  }
}
