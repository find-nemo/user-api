import { Container } from 'inversify';

import { PluginManager } from './plugins.config';
import { InterfaceErrorHandlerPlugin } from './plugins/error-handler.interface';
import { ErrorHandlerPlugin } from './plugins/error-handler.plugin';
import { InterfaceRequestValidationMiddleware } from './plugins/request-validation.interface';
import { RequestValidationMiddleware } from './plugins/request-validation.plugin';
import { InterfaceSecretManagerPlugin } from './plugins/secret-manager.interface';
import { SecretManagerPlugin } from './plugins/secret-manager.plugin';
import { InterfaceSequelizePlugin } from './plugins/sequelize.interface';
import { SequelizePlugin } from './plugins/sequelize.plugin';
import { ChildController } from './server/controllers/child.controller';
import { HealthController } from './server/controllers/health.controller';
import { SwaggerController } from './server/controllers/swagger.controller';
import { UserTypeController } from './server/controllers/user-type.controller';
import { UserController } from './server/controllers/user.controller';
import { FirebaseTokenMiddleware } from './server/middlewares/firebase-token.middleware';
import { SYMBOLS } from './server/models/error-symbol';
import { AddressDataAccess } from './server/repository/address.da';
import { AddressDataAccessInterface } from './server/repository/address.interface';
import { ChildDataAccess } from './server/repository/child.da';
import { ChildDataAccessInterface } from './server/repository/child.interface';
import { UserTypeDataAccess } from './server/repository/user-type.da';
import { UserTypeDataAccessInterface } from './server/repository/user-type.interface';
import { UserDataAccess } from './server/repository/user.da';
import { UserDataAccessInterface } from './server/repository/user.interface';
import { AddressServiceInterface } from './server/services/address.interface';
import { AddressService } from './server/services/address.service';
import { ChildServiceInterface } from './server/services/child.interface';
import { ChildService } from './server/services/child.service';
import { UserTypeServiceInterface } from './server/services/user-type.interface';
import { UserTypeService } from './server/services/user-type.service';
import { UserServiceInterface } from './server/services/user.interface';
import { UserService } from './server/services/user.service';
import { ValidateTokenUtil } from './server/utils/validate-user-token.util';
import { FirestoreServiceInterface } from './server/services/firestore.interface';
import { FirestoreService } from './server/services/firestrore.service';
import { AddressController } from './server/controllers/address.controller';

const appContainer = new Container();
appContainer
  .bind<PluginManager>(nameof<PluginManager>())
  .to(PluginManager)
  .inSingletonScope();
appContainer
  .bind<InterfaceErrorHandlerPlugin>(nameof<InterfaceErrorHandlerPlugin>())
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  .toFactory<InterfaceErrorHandlerPlugin>((): any => {
    return (name: string): ErrorHandlerPlugin => {
      name = name || 'UNKNOWN';
      const service = appContainer.get<ErrorHandlerPlugin>(
        SYMBOLS.DiagnosticsInstance
      );
      service.setName(name);
      return service;
    };
  });
appContainer
  .bind<InterfaceErrorHandlerPlugin>(SYMBOLS.DiagnosticsInstance)
  .to(ErrorHandlerPlugin)
  .inTransientScope();
appContainer
  .bind<InterfaceRequestValidationMiddleware>(
    nameof<InterfaceRequestValidationMiddleware>()
  )
  .to(RequestValidationMiddleware)
  .inTransientScope();
appContainer
  .bind<SwaggerController>(nameof<SwaggerController>())
  .to(SwaggerController);
appContainer
  .bind<ValidateTokenUtil>(nameof<ValidateTokenUtil>())
  .to(ValidateTokenUtil);
appContainer
  .bind<HealthController>(nameof<HealthController>())
  .to(HealthController);
appContainer
  .bind<FirebaseTokenMiddleware>(nameof<FirebaseTokenMiddleware>())
  .to(FirebaseTokenMiddleware)
  .inSingletonScope();
appContainer
  .bind<InterfaceSecretManagerPlugin>(nameof<InterfaceSecretManagerPlugin>())
  .to(SecretManagerPlugin)
  .inSingletonScope();
appContainer
  .bind<InterfaceSequelizePlugin>(nameof<InterfaceSequelizePlugin>())
  .to(SequelizePlugin)
  .inSingletonScope();
appContainer
  .bind<AddressDataAccessInterface>(nameof<AddressDataAccessInterface>())
  .to(AddressDataAccess)
  .inSingletonScope();
appContainer
  .bind<AddressServiceInterface>(nameof<AddressServiceInterface>())
  .to(AddressService)
  .inSingletonScope();
appContainer
  .bind<AddressController>(nameof<AddressController>())
  .to(AddressController);
appContainer
  .bind<UserDataAccessInterface>(nameof<UserDataAccessInterface>())
  .to(UserDataAccess)
  .inSingletonScope();
appContainer
  .bind<UserServiceInterface>(nameof<UserServiceInterface>())
  .to(UserService)
  .inSingletonScope();
appContainer.bind<UserController>(nameof<UserController>()).to(UserController);
appContainer
  .bind<UserTypeDataAccessInterface>(nameof<UserTypeDataAccessInterface>())
  .to(UserTypeDataAccess)
  .inSingletonScope();
appContainer
  .bind<UserTypeServiceInterface>(nameof<UserTypeServiceInterface>())
  .to(UserTypeService)
  .inSingletonScope();
appContainer
  .bind<UserTypeController>(nameof<UserTypeController>())
  .to(UserTypeController);
appContainer
  .bind<ChildDataAccessInterface>(nameof<ChildDataAccessInterface>())
  .to(ChildDataAccess)
  .inSingletonScope();
appContainer
  .bind<ChildServiceInterface>(nameof<ChildServiceInterface>())
  .to(ChildService)
  .inSingletonScope();
appContainer
  .bind<ChildController>(nameof<ChildController>())
  .to(ChildController);
appContainer
  .bind<FirestoreServiceInterface>(nameof<FirestoreServiceInterface>())
  .to(FirestoreService)
  .inSingletonScope();
export { appContainer };
